class User {
    constructor(name, email, password){
        this.name = name;
        this.email = email;
        this.password = password;        
    }

    get getName() {
        return this.name;
    }

    /**
     * @param {string} value
     */
    set setName(value) {
        this.name = value;
        return this.name;
    }

    static {
        this.authToken = "jkahfjh";
    }
}

const sumon = new User('sumon', 'sumon@email.com', 'agvsavc');

console.log(sumon.getName);
sumon.setName = "SUMON";
console.log(sumon.getName);
console.log(User.authToken)