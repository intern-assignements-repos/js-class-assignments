// Calculate the sum of odd numbers greater than 10 and multiples of 3 from a given array

const arr = [11, 23, 25, 27, 90, 30, 33, 21, 66, 78, 17];

function cal(arr) {
  n = arr.length;
  sum = 0;
  for (let i = 0; i < n; i++) {
    if (arr[i] > 10 && arr[i] % 2 !== 0 && arr[i] % 3 === 0) {
      sum += arr[i];
    }
  }

  return sum;
}

console.log(cal(arr));
