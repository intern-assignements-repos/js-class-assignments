// Rotate an array of numbers to the left by the given number.
// For e.g. rotateArray(2) rotates the array to the left by 2 positions

function rotateArray(pos) {
  const arr = [2, 7, 8, 20, 14, 7];
  let res = [];
  let n = arr.length,
    i = 0;

  for (i = pos; i < n; i++) 
    res.push(arr[i]);    
  

  for(i=0; i<pos; i++)
    res.push(arr[i]);

  return res;
}

console.log(rotateArray(2));
