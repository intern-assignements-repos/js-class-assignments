// Create a function that reverses an array

const arr = ["a", "b", "c", "d", "e"];

function rev(arr) {
  let n = arr.length;
  let temp;
  for (let i = 0; i < n / 2; i++) {
    temp = arr[i];
    arr[i] = arr[n - i - 1];
    arr[n - i - 1] = temp;
  }

  console.log(arr);
}

rev(arr);
