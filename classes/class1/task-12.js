// Find the element having the highest frequency in an array of numbers

const arr = [1, 2, 2, 3, 1, 2, 4, 5, 6, 7, 8];

function highFreq(arr) {
  let res;
  let max = 0;
  let d = {};

  for (let i = 0; i < arr.length; i++) {
    if (d[arr[i]] !== undefined) {
      d[arr[i]] += 1;
    } else {
      d[arr[i]] = 1;
    }
  }

  for (let i in d) {
    if (d[i] > max) {
      max = d[i];
      res = i;
    }
  }

  return res;
}

console.log(highFreq(arr));
