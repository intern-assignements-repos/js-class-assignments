// Given a binary string, convert it into the equivalent decimal number.

const bin = '101';

function binToDecimal(bin){
    let res = 0, n = bin.length, j = 0;

    for(let i = n-1; i >= 0; i--){
        res += (parseInt(bin[i]) * (2**j));
        j++;
    }

    return res;
}

console.log(binToDecimal(bin));