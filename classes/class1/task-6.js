// Remove the spaces from a string

const str = "Remove the spaces from a string";

function removeSpaces(str) {
  let res = "";
  let n = str.length;
  let ind = 0;
  for (let i = 0; i < n; i++) {
    if (str[i] === " ") {
      res += customSubString(str, ind, i);
      ind = i + 1;
    }
    if (i === n - 1) {
      res += customSubString(str, ind, n);
    }
  }
  return res;
}

function customSubString(str, startIndex, endIndex) {
  let temp = "";
  for (let i = startIndex; i < endIndex; i++) {
    temp += str[i];
  }

  return temp;
}

console.log(removeSpaces(str));
