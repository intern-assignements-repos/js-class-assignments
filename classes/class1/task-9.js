// Find the missing number in an array of length n, having elements from 1 to n

const n = 5;
const arr = [3, 2, 2, 1, 3];

function findMissingNumber(arr, n) {
  let i = 1;
  let d = {};

  while (i <= n) {
    d[i] = 0;
    i++;
  }

  i = 0;

  while (i < n) {
    d[arr[i]] += 1;
    i++;
  }

  for (let j in d) {
    if (d[j] === 0) return j;
  }

  return 0;
}

console.log(findMissingNumber(arr, n));
