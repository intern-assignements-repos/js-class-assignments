// A vehicle needs 10 times the amount of fuel than the distance it travels. 
// However, it must always carry a minimum of 100 fuel before setting off. 
// Create a function which calculates the amount of fuel it needs, given the distance.
// E.g. caculateFuel(15)  —> 150


const distance = 5;

function calculateFuel(dis) {
    return (dis*10) < 100 ? 100 : dis*10;
}

console.log(calculateFuel(distance));