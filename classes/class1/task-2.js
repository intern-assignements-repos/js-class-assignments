// Create a length converter function: kms to miles

const factor = 0.62137119;

function converter(kms) {
  return kms * factor;
}

let value = 5;

console.log(converter(value));
