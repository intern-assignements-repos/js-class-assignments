// Return the number of vowels in a string

const str = "Return the number of vowels in a string";
const vowels = ["a", "e", "i", "o", "u"];

function countVowels(str) {
  let count = 0;
  for (i in str) if (customIncludes(vowels, str[i])) count++;

  return count;
}

function customIncludes(arr, charToCheck) {
  let n = arr.length;
  for (let i = 0; i < n; i++) if (arr[i] === charToCheck) return true;

  return false;
}

console.log(countVowels(str));
