// Create a random number generator of length 4

const n = 4;
const PI = 3.141592653589793;
let lastRand, randNum;

function rand(){
    while(randNum == lastRand)
        randNum = (new Date().getTime()*PI)%1;

    return lastRand = randNum;
}

function randomNum(n) {
    let res = 0;

    let randumValue = rand();
    res = parseInt(1000 + randumValue/10**(randumValue.toString.length - 1)*9000);

    return res;
}

console.log(randomNum(n));

// console.log(Math.random());
// console.log(parseInt(1000 + Math.random() * 9000));
