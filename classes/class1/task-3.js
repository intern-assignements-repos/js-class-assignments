// Convert days into seconds

const factor = 86400;

function daysToSeconds(days){
    return days*factor;
}

console.log(daysToSeconds(5));