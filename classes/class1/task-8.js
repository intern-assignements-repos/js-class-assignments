// Reverse each word of a given sentence

const str = "Reverse each word of a given sentence";

function revEachWord(str) {
  let res = "",
    temp = "";
  let n = str.length;
  let arr = [];
  for (let i = n - 1; i >= 0; i--) {
    if (str[i] === " ") {
      arr.push(temp);
      temp = "";
    } else if (i === 0) {
        arr.push(temp + str[0]);        
    } else {
      temp += str[i];
    }
  }

  for (let j = arr.length - 1; j >= 0; j--) {
    res += arr[j] + " ";
  }

  return res;
}

console.log(revEachWord(str));
