// Check if the given two strings are anagrams of each other or not.
// For e.g. ‘care’ and ‘race’

const test1 = 'care';
const test2 = 'race';

function checkAnagrams(word1, word2){    
    let firstSet = new Set();

    if(word1.length !== word2.length)
        return false;

    for(let i = 0; i < word1.length; i++){
        if(!firstSet.has(word1[i])){
            firstSet.add(word1[i]);
        }
    }

    for(let i = 0; i < word2.length; i++){
        if(!firstSet.has(word2[i]))
            return false;
    }

    return true;
}

console.log(checkAnagrams(test1, test2))