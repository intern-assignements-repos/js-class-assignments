// Find the even duplicate elements in an array of numbers

const arr = [1, 2, 1, 2, 3, 7, 8, 10, 5, 5, 10];

function duplicateFinder(arr) {
    let n = arr.length;
    let d = {};
    for(let i = 0; i<n; i++){
        if(d[arr[i]] !== undefined){
            d[arr[i]] += 1;
        } else {
            d[arr[i]] = 1;            
        }
    }

    let res = [];

    for(let i in d){
        if(i%2==0 && d[i] > 1){
            res.push(i);
        }
    }    

    return res;
    
}

console.log(duplicateFinder(arr));
