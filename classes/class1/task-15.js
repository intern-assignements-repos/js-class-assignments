// Sort an array which consists of elements: 0, 1 & 2

const arr = [1, 2, 2, 1, 0, 1, 2, 0, 0, 1, 0, 1, 2, 0];

function sortArr(arr) {
  let res = [], n = arr.length;
  let zero = 0,
    one = 0,
    two = 0;

  for (let i in arr) {
    if (arr[i] === 0) zero++;
    else if (arr[i] === 1) one++;
    else two++;
  }

  for (let i = 0; i < n; i++) {    
    if(zero > 0){
        res.push(0);
        zero--;
    }else if(one > 0){
        res.push(1);
        one--;
    } else {
        res.push(2);
        two--;
    }
  }

  return res;
}

console.log(sortArr(arr));
