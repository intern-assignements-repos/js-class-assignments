// Create a function that returns base-2 (binary) representation of a base-10 ( decimal ) string number.
// E.g. binary(5) —> 101

const decimal = 5;

function decimalToBinary(num) {
  let res = 0,
    rem,
    i = 1;

  while (num !== 0) {
    rem = num % 2;
    num = parseInt(num / 2);
    res = res + rem * i;
    i = i * 10;
  }

  return res;
}

console.log(decimalToBinary(decimal));
