// imports :
// importing read and write functions :
const { readFileSyncly, readFileASyncly } = require("./fileRead");
const { writeFileSyncly, writeFileASyncly } = require("./fileWrite");

// imports sync with callBacks :
const {
  syncDataFetchWithCallBack,
  syncDataParseWithCallBack,
  syncTransformDataWithCallBack,
  syncTransformDataAndUpdateWithCallBack,
} = require("./syncWithCallBack-V2");

// imports sync with promises :
const {
  syncDataFetchWithPromises,
  syncDataParseWithPromises,
  syncTransformDataWithPromises,
  syncTransformDataAndUpdateWithPromises,
} = require("./syncWithPromises-V2");

// imports async with callBacks :
const {
  asyncDataFetchWithCallBack,
  asyncDataParseWithCallBack,
  asyncTransformDataWithCallBack,
  asyncTransformDataAndUpdateWithCallBack,
} = require("./asyncWithCallBack-V2");

// imports sync with promises :
const {
  asyncDataFetchWithPromises,
  asyncDataParseWithPromises,
  asyncTransformDataWithPromises,
  asyncTransformDataAndUpdateWithPromises,
} = require("./asyncWithPromises-V2");

// imports sync with promises :
const {
  asyncAwaitDataFetch,
  asyncAwaitDataParse,
  asyncAwaitDataTransform,
  asyncAwaitDataTransformAndUpdate
} = require("./asyncAwait-V2");

// file url :
const fileURL = "./data/PERSON_DATA.json";

// sync data parse with callback :
syncDataFetchWithCallBack(fileURL, readFileSyncly);

// sync data parse with callback :
syncDataParseWithCallBack(fileURL, readFileSyncly);

// sync data transform with callback :
syncTransformDataWithCallBack(fileURL, readFileSyncly);

// sync data transform and update with callback :
syncTransformDataAndUpdateWithCallBack(fileURL, readFileSyncly, writeFileSyncly);

// sync data fetch with Promises :
syncDataFetchWithPromises(fileURL)
  .then((data) => console.log(data))
  .catch((err) => console.error(err));

// sync data parse with Promises :
syncDataParseWithPromises(fileURL)
  .then((data) => console.log(data))
  .catch((err) => console.error(err));

// sync data transform with Promises :
syncTransformDataWithPromises(fileURL)
  .then((data) => console.log(data))
  .catch((err) => console.error(err));

// sync data transform and update with promises :
syncTransformDataAndUpdateWithPromises(fileURL)
  .then((data) =>
    console.log(
      writeFileSyncly("data/updates", data)
        ? ("Written Successfully :", data)
        : new Error("Writing Failed")
    )
  )
  .catch((err) => console.log("synchronously with Promises (err):", err));

// async data fetch with callBack :
asyncDataFetchWithCallBack(fileURL, readFileASyncly);

//async data parse with callBack :
asyncDataParseWithCallBack(fileURL, readFileASyncly);

// async data transform with Promises :
asyncTransformDataWithCallBack(fileURL, readFileASyncly)

// async data transform and update with callback :
asyncTransformDataAndUpdateWithCallBack(fileURL, readFileASyncly, writeFileASyncly);

// async data fetch with Promises :
asyncDataFetchWithPromises(fileURL)
  .then(async (data) => console.log(await data))
  .catch(async (err) => console.error(await err));

// async data parse with Promises :
asyncDataParseWithPromises(fileURL)
  .then(async (data) => console.log(await data))
  .catch(async (err) => console.error(await err));

// async data transform with Promises :
asyncTransformDataWithPromises(fileURL)
  .then(async (data) => console.log(await data))
  .catch(async (err) => console.error(await err));

// async data transform and update with promises :
asyncTransformDataAndUpdateWithPromises(fileURL)
  .then(async (data) =>
    console.log(
     await writeFileASyncly("data/updates", await data)
        ? ("Written Successfully :", await data)
        : new Error("Writing Failed")
    )
  )
  .catch(async (err) => console.log("asynchronously with Promises (err):", await err));

// async await data fetch :
asyncAwaitDataFetch(fileURL);

// async await data parse :
asyncAwaitDataParse(fileURL);

// async await data transform :
asyncAwaitDataTransform(fileURL);

// async await data transform and update :
asyncAwaitDataTransformAndUpdate(fileURL);