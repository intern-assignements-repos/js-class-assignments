const { readFileASyncly } = require("./fileRead");
const { writeFileASyncly } = require("./fileWrite");

// fetch data - with async and await only :
const asyncAwaitDataFetch = async (url) => {
  try {
    const jsonData = await readFileASyncly(url);
    console.log("Using async await :", jsonData);
  } catch (err) {
    console.error(await err);
  }
};

// parse its data and filter only the objects having age greater than 25
const asyncAwaitDataParse = async (url) => {
  try {
    const jsonData = await readFileASyncly(url);
    console.log(
      jsonData
        .filter((item) => item.age > 25)
        .map((element) => {
          console.log(element);
        })
    );
  } catch (err) {
    console.error(await err);
  }
};

// transform ‘firstName’ and ‘lastName’ to capitalize
const asyncAwaitDataTransform = async (url) => {
  try {
    const jsonData = await readFileASyncly(url);
    console.log(
      jsonData
        .filter((item) => {
          item.firstName =
            item.firstName.charAt(0).toUpperCase() + item.firstName.slice(1);
          item.lastName =
            item.lastName.charAt(0).toUpperCase() + item.lastName.slice(1);
          return item;
        })
        .map((item) => console.log(item))
    );
  } catch (err) {
    console.error(await err);
  }
};

// update the file with this transformed data.
const asyncAwaitDataTransformAndUpdate = async (url) => {
  try {
    const jsonData = await readFileASyncly(url);
    const updatedData = jsonData.filter((item) => {
      item.firstName =
        item.firstName.charAt(0).toUpperCase() + item.firstName.slice(1);
      item.lastName =
        item.lastName.charAt(0).toUpperCase() + item.lastName.slice(1);
      return item;
    });

    console.log(
      (await writeFileASyncly("./data/updates", updatedData))
        ? ("Written Successfully", updatedData)
        : "Error : Write Failed"
    );
  } catch (err) {
    console.error(err);
  }
};


module.exports = {
    asyncAwaitDataFetch,
    asyncAwaitDataParse,
    asyncAwaitDataTransform,
    asyncAwaitDataTransformAndUpdate
}