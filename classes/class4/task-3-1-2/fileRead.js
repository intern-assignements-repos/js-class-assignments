const fs = require("fs");

const readFileSyncly = (url) =>{
    try{
        const data = fs.readFileSync(url, { encoding: "utf8", flag: "r" });
        return JSON.parse(data);
    }catch(err){
        console.error(err);
    }
}

const readFileASyncly = async (url) => {
    try{
        const data = await fs.promises.readFile(url, {encoding: "utf8",flag: "r"});
        return JSON.parse(data);
    }catch(err){
        console.error(err);
    }
}

module.exports = {
    readFileASyncly,
    readFileSyncly
}