const fs = require("fs");

// write syncly

const writeFileSyncly = (filePath, data) => {
  try {
    fs.writeFileSync(
      `${filePath}/UPDATED_PERSON_DATA_${new Date().getTime()}.json`,
      JSON.stringify(data),
      (err) => {
        console.error(err);
        throw new Error("Error : Updation failed");
      }
    );

    return true;
  } catch (err) {
    console.error(err);
    return false;
  }
};

const writeFileASyncly = async (filePath, data) => {
  try {
    await fs.promises.writeFile(
      `${filePath}/UPDATED_PERSON_DATA_${new Date().getTime()}.json`,
      JSON.stringify(data),
      (err) => {
        console.error(err);
        throw new Error("Error : Updation failed");
      }
    );
    return true;
  } catch (err) {
    console.error(err);
    return false;
  }
};

module.exports = {
  writeFileSyncly,
  writeFileASyncly,
};
