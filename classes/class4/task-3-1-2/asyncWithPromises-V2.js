const { readFileASyncly } = require("./fileRead");

// fetch data - Synchronously with Promises :

const asyncDataFetchWithPromises = async (fileURL) => {
  return new Promise(async (resolve, reject) => {
    try {
      return resolve(await readFileASyncly(fileURL));
    } catch (err) {
      return reject(err);
    }
  });
};

// parse its data and filter only the objects having age greater than 25
const asyncDataParseWithPromises = async (fileURL) => {
  return new Promise(async (resolve, reject) => {
    try {
      const data = await readFileASyncly(fileURL);
      const parsedData = data.filter((item) => item.age > 25);
      return resolve(parsedData);
    } catch (err) {
      return reject(err);
    }
  });
};

// // transform ‘firstName’ and ‘lastName’ to capitalize
const asyncTransformDataWithPromises = async (fileURL) => {
  return new Promise(async (resolve, reject) => {
    try {
      const data = await readFileASyncly(fileURL);
      const updatedData = data.filter((item) => {
        item.firstName =
          item.firstName.charAt(0).toUpperCase() + item.firstName.slice(1);
        item.lastName =
          item.lastName.charAt(0).toUpperCase() + item.lastName.slice(1);
        return item;
      });
      return resolve(updatedData);
    } catch (err) {
      return reject(err);
    }
  });
};

// // update the file with this transformed data
const asyncTransformDataAndUpdateWithPromises = async (fileURL) => {
  return new Promise(async (resolve, reject) => {
    try {
      const data = await readFileASyncly(fileURL);
      const updatedData = data.filter((item) => {
        item.firstName =
          item.firstName.charAt(0).toUpperCase() + item.firstName.slice(1);
        item.lastName =
          item.lastName.charAt(0).toUpperCase() + item.lastName.slice(1);
        return item;
      });
      return resolve(updatedData);
    } catch (err) {
      return reject(err);
    }
  });
};

// exporting the modules
module.exports = {
  asyncDataFetchWithPromises,
  asyncDataParseWithPromises,
  asyncTransformDataWithPromises,
  asyncTransformDataAndUpdateWithPromises,
};
