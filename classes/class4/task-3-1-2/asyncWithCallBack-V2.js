// async data fetch with callBack :
const asyncDataFetchWithCallBack = async (url, readFileASyncly) => {
  try {
    console.log(await readFileASyncly(url));
  } catch (err) {
    console.error(err);
  }
};

// async data parse with callBack :
const asyncDataParseWithCallBack = async (url, readFileASyncly) => {
  try {
    const jsonData = await readFileASyncly(url);
    console.log(jsonData.filter((item) => item.age > 25));
  } catch (err) {
    console.error(err);
  }
};

// transform ‘firstName’ and ‘lastName’ to capitalize - asyncly with callBacks
const asyncTransformDataWithCallBack = async (url, readFileASyncly) => {
  try {
    const jsonData = await readFileASyncly(url);
    console.log(
      jsonData.filter((item) => {
        item.firstName =
          item.firstName.charAt(0).toUpperCase() + item.firstName.slice(1);
        item.lastName =
          item.lastName.charAt(0).toUpperCase() + item.lastName.slice(1);
        return item;
      })
    );
  } catch (err) {
    console.error(err);
  }
};

// update the file with this transformed data - asyncly with callBack
const asyncTransformDataAndUpdateWithCallBack = async (
  url,
  readFileASyncly,
  writeFileASyncly
) => {
  try {
    const jsonData = await readFileASyncly(url);
    const updatedData = jsonData.filter((item) => {
      item.firstName =
        item.firstName.charAt(0).toUpperCase() + item.firstName.slice(1);
      item.lastName =
        item.lastName.charAt(0).toUpperCase() + item.lastName.slice(1);
      return item;
    });

    console.log(
      (await writeFileASyncly("./data/updates", updatedData))
        ? ("Written Successfully", updatedData)
        : "Error : Write Failed"
    );
  } catch (err) {
    console.error(err);
  }
};

module.exports = {
  asyncDataFetchWithCallBack,
  asyncDataParseWithCallBack,
  asyncTransformDataWithCallBack,
  asyncTransformDataAndUpdateWithCallBack,
};
