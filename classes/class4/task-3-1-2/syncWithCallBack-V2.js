// const { readFileSyncly } = require("./fileRead");
// const { writeFileSyncly } = require("./fileWrite");

// fetch data - Synchronously with callBack :

const syncDataFetchWithCallBack = (url, readFileSyncly) => {
  try {
    console.log(readFileSyncly(url));
  } catch (err) {
    console.error(err);
  }
};

// parse its data and filter only the objects having age greater than 25

const syncDataParseWithCallBack = (url, readFileSyncly) => {
  try {
    const jsonData = readFileSyncly(url);
    console.log(jsonData.filter((item) => item.age > 25));
  } catch (err) {
    console.error(err);    
  }
};

// transform ‘firstName’ and ‘lastName’ to capitalize

const syncTransformDataWithCallBack = (url, readFileSyncly) => {
  try {
    const jsonData = readFileSyncly(url);
    console.log(jsonData.filter((item) => {
      item.firstName =
        item.firstName.charAt(0).toUpperCase() + item.firstName.slice(1);
      item.lastName =
        item.lastName.charAt(0).toUpperCase() + item.lastName.slice(1);
      return item;
    }));
  } catch (err) {
    console.error(err);
  }
};

// update the file with this transformed data.

const syncTransformDataAndUpdateWithCallBack = (url, readFileSyncly, writeFileSyncly) => {
  try {
    const jsonData = readFileSyncly(url);    
    const updatedData = jsonData.filter((item) => {
      item.firstName =
        item.firstName.charAt(0).toUpperCase() + item.firstName.slice(1);
      item.lastName =
        item.lastName.charAt(0).toUpperCase() + item.lastName.slice(1);
      return item;
    });

    console.log(
      writeFileSyncly("./data/updates", updatedData) ? ("Written Successfully", updatedData) : "Error : Write Failed"
    );    
  } catch (err) {
    console.error(err);
  }
};

module.exports = {
  syncDataFetchWithCallBack,
  syncDataParseWithCallBack,
  syncTransformDataWithCallBack,
  syncTransformDataAndUpdateWithCallBack
};
