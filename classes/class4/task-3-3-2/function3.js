const function2 = require("./function2");

// function3 made with Promises :
const function3 = async (ms) => {
  try {
    return new Promise(async (resolve, reject) => {
      function2(ms)
        .then((val) => {          
          setTimeout(() => {
            resolve(val + 3);
          }, ms);
        })
        .catch((err) => console.error(err));
    });
  } catch (err) {
    console.error(err);
  }
};

module.exports = function3;
