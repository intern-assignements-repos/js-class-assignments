const function1 = require('./function1');

// function3 made with Promises :
const function2 = async (ms) => {
    try {
      return new Promise(async (resolve, reject) => {
        const value = await function1(ms);        
        setTimeout(() => {
          resolve(value + 2);
        }, ms);
      });
    } catch (err) {
      console.error(err);
    }
  };

module.exports = function2;