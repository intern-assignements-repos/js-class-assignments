const function3 = require("./function3");

// function4 made with callBack
const function4 = async (ms, callBack) => {
  try {    
    function3(ms)
      .then((val) => {    
        setTimeout(() => callBack("", val + 4), ms);
      })
      .catch((err) => {
        return callBack(err, val + 4);
      });
  } catch (err) {
    console.error(err);
  }
};

module.exports = function4;
