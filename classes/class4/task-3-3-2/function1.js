// function3 made with async Promises :
const function1 = async (ms) => {
  try {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(1);
      }, ms);
    });
  } catch (err) {
    console.error(err);
  }
};

module.exports = function1;
