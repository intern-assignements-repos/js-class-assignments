// Write 4 async functions using setTimeout(), each of these functions returns a number.
// These functions are dependent on each other in the following manner:
// Function2 depends on function1
// Function3 depends on function2
// Function4 depends on function3
// In the end we want to calculate the sum of all these numbers returned by the functions.
// Do this using all three ways: callbacks, promises and async await.

const function4 = require('./function4');

const callBack = (err, val) => {
    if(err)
      console.error(err)
    else
      console.log(val);
  };
  
function4(1000, callBack);