// deleteUser: It takes 1 param ( emailId ) and deletes the user from our file.

const getData = require("./getData");
const setData = require("./setData");

const deleteUser = async (email) => {
  try {
    const data = await getData();
    let user;
    const newData = data.filter(element => {
        if(element.email !== email)
            return element;
        else
            user = element;
    })

    if(user.length !== 0)
        console.log(await setData(newData) ? ("Success :", newData) : "Error : Deletion Failed");
    else
        throw new Error("Invalid credentials");
    
  } catch (err) {
    console.error(err);
  }
};

module.exports = deleteUser;
