const fs = require("fs");
const fileURL = "./data/AUTH_DATA.json";

const setData = async (data) => {
  try {
    await fs.promises.writeFile(
      fileURL,
      JSON.stringify(data),
      (err) => console.error(err)
    );
    return true;
  } catch (err) {    
    console.error(err);
    return false;
  }
};

module.exports = setData;
