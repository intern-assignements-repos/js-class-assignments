// updateProfile: It takes 3 params: ( emailId, age and address ) and
// updates the age and address of the user with a given emailId.
// It throws an error if user does not exist in our file
// Returns the updated data if user exists


const getData = require('./getData');
const setData = require('./setData');

const updateProfile = async (email, age, address) => {
    try{        
        const data = await getData();
        const user = data.filter(element => {
            if(element.email === email){
                element.age = age;
                element.address = address;
                return element;
            }
        });

        if(user.length !== 0)
            console.log(await setData(data) ? ("Success :", data) : "Error : Updation Failed");
        else
            throw new Error("Invalid credentials");

    }catch(err) {
        console.error(err);
    }
}

module.exports = updateProfile;