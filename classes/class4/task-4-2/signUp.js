const getData = require('./getData');
const setData = require('./setData');

const signUp = async (newUserData) => {
    try{
        const data = await getData();
        const user = data.filter((element) => {
            if(element.email === newUserData.email)
                return element;                
        })

        if(user.length === 0){
            newUserData = {"id": parseInt(data[data.length-1].id)+1, ...newUserData}

            const updatedData = [...data, newUserData];
            console.log(await setData(updatedData) ? ("Success :", updatedData) : "Error : Updation Failed");
        }else{
            throw new Error("An user with same email id already exist");
        }

    }catch(err){
        console.error(err);
    }
}

module.exports = signUp;