const fs = require('fs');
const fileURL = "./data/AUTH_DATA.json"

const getData = async () => {
    try{
        const data = await fs.promises.readFile(fileURL, {encoding: "utf8",flag: "r"});
        return JSON.parse(data);
    }catch(err){
        console.error(err);
    }
}

module.exports = getData;