// SignIn: It takes 2 params: (emailId and password) and checks if this user exists in our file or not. 
    // It throws an error if user does not exist in our file
    // Returns the user data if it exists in our fie

const getData = require('./getData');

const signIn = async (email, password) => {
    try{
        const data = await getData();
        const user = data.filter(element => {
            if(element.email === email && element.password === password){
                return element
            }
        });

        if(user.length !== 0)
            console.log(user);
        else
            throw new Error("User Not Exists");
        
    }catch(err){
        console.error(err);        
    }
}

module.exports = signIn