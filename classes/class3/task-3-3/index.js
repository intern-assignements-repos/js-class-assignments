// Write 4 async functions using setTimeout(), each of these functions returns a number.
// These functions are dependent on each other in the following manner:
// Function2 depends on function1
// Function3 depends on function2
// Function4 depends on function3
// In the end we want to calculate the sum of all these numbers returned by the functions.
// Do this using all three ways: callbacks, promises and async await.

const function1 = async (ms) => {
  try {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(1);
      }, ms);
    });
  } catch (err) {
    console.error(err);
  }
};

const function2 = async (ms) => {
  try {
    return new Promise(async (resolve, reject) => {
      const value = await function1(ms);

      setTimeout(() => {
        resolve(value + 2);
      }, ms);
    });
  } catch (err) {
    console.error(err);
  }
};

const function3 = async (ms) => {
  try {
    return new Promise(async (resolve, reject) => {
      const value = await function2(ms);

      setTimeout(() => {
        resolve(value + 3);
      }, ms);
    });
  } catch (err) {
    console.error(err);
  }
};

const function4 = async (ms) => {
  try {
    const value = await function3(ms);

    console.log(value + 4);
  } catch (err) {
    console.error(err);
  }
};

function4(1000);