const fs = require("fs");
const fileURL = "./data/PERSON_DATA.json";

// fetch data - Asynchronously with Promises :

const asyncDataFetchWithPromises = new Promise(async (resolve, reject) => {
  try {
    const data = await fs.promises.readFile(fileURL, {
      encoding: "utf8",
      flag: "r",
    });
    return resolve(JSON.parse(data));
  } catch (err) {
    return reject(err);
  }
});

asyncDataFetchWithPromises
  .then((data) => console.log("asynchronously with Promises (data):", data))
  .catch((err) => console.log("asynchronously with Promises (err):", err));

// parse its data and filter only the objects having age greater than 25

const asyncDataParseWithPromises = new Promise(async (resolve, reject) => {
  try {
    const data = await fs.promises.readFile(fileURL, {
      encoding: "utf8",
      flag: "r",
    });
    const parsedData = JSON.parse(data).filter((item) => item.age > 25);
    return resolve(parsedData);
  } catch (err) {
    return reject(err);
  }
});

asyncDataParseWithPromises
  .then((data) => console.log("asynchronously with Promises (data):", data))
  .catch((err) => console.log("asynchronously with Promises (err):", err));

// transform ‘firstName’ and ‘lastName’ to capitalize

const asyncTransformDataWithPromises = new Promise(async (resolve, reject) => {
  try {
    const data = await fs.promises.readFile(fileURL, {
      encoding: "utf8",
      flag: "r",
    });
    const updatedData = JSON.parse(data).filter((item) => {
      item.firstName =
        item.firstName.charAt(0).toUpperCase() + item.firstName.slice(1);
      item.lastName =
        item.lastName.charAt(0).toUpperCase() + item.lastName.slice(1);
      return item;
    });
    return resolve(updatedData);
  } catch (err) {
    return reject(err);
  }
});

asyncTransformDataWithPromises
  .then((data) => console.log("asynchronously with Promises (data):", data))
  .catch((err) => console.log("asynchronously with Promises (err):", err));

// update the file with this transformed data.

const asyncTransformDataAndUpdateWithPromises = new Promise(
  async (resolve, reject) => {
    try {
      const data = await fs.promises.readFile(fileURL, {
        encoding: "utf8",
        flag: "r",
      });
      const updatedData = JSON.parse(data).filter((item) => {
        item.firstName =
          item.firstName.charAt(0).toUpperCase() + item.firstName.slice(1);
        item.lastName =
          item.lastName.charAt(0).toUpperCase() + item.lastName.slice(1);
        return item;
      });
      return resolve(updatedData);
    } catch (err) {
      return reject(err);
    }
  }
);

asyncTransformDataAndUpdateWithPromises
  .then((data) =>
    fs.writeFileSync(
      `./data/updates/UPDATED_PERSON_DATA_${new Date().getTime()}.json`,
      JSON.stringify(data),
      (err) => console.error(err)
    )
  )
  .catch((err) => console.log("asynchronously with Promises (err):", err));
