const fs = require("fs");
const fileURL = "./data/PERSON_DATA.json";

// fetch data - with async and await only :

const asyncAwaitDataFetch = async (url) => {
  try {
    const jsonData = await fs.promises.readFile(url, {
      encoding: "utf8",
      flag: "r",
    });
    console.log("Using async await :", JSON.parse(jsonData));
  } catch (err) {
    console.error(err);
  }
};

// asyncAwaitDataFetch(fileURL);

// parse its data and filter only the objects having age greater than 25

const asyncAwaitDataParse = async (url) => {
  try {
    const jsonData = await fs.promises.readFile(url, {
      encoding: "utf8",
      flag: "r",
    });
    JSON.parse(jsonData)
      .filter((item) => item.age > 25)
      .map((element) => {
        console.log(element);
      });
  } catch (err) {
    console.error(err);
  }
};

// asyncAwaitDataParse(fileURL);

// transform ‘firstName’ and ‘lastName’ to capitalize

const asyncAwaitDataTransform = async (url) => {
  try {
    const jsonData = await fs.promises.readFile(url, {
      encoding: "utf8",
      flag: "r",
    });
    JSON.parse(jsonData)
      .filter((item) => {
        item.firstName =
          item.firstName.charAt(0).toUpperCase() + item.firstName.slice(1);
        item.lastName =
          item.lastName.charAt(0).toUpperCase() + item.lastName.slice(1);
        return item;
      })
      .map((item) => console.log(item));
  } catch (err) {
    console.error(err);
  }
};

asyncAwaitDataTransform(fileURL);

// update the file with this transformed data.

const asyncAwaitDataTransformAndUpdate = async (url) => {
  try {
    const jsonData = await fs.promises.readFile(url, {
      encoding: "utf8",
      flag: "r",
    });
    const updatedData = JSON.parse(jsonData).filter((item) => {
      item.firstName =
        item.firstName.charAt(0).toUpperCase() + item.firstName.slice(1);
      item.lastName =
        item.lastName.charAt(0).toUpperCase() + item.lastName.slice(1);
      return item;
    });

    await fs.promises.writeFile(
      `./data/updates/UPDATED_PERSON_DATA_${new Date().getTime()}.json`,
      JSON.stringify(updatedData),
      (err) => console.error(err)
    );
  } catch (err) {
    console.error(err);
  }
};

asyncAwaitDataTransformAndUpdate(fileURL);
