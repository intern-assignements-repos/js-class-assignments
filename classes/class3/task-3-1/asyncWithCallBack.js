const fs = require("fs");
const fileURL = "./data/PERSON_DATA.json";

// fetch data - Asynchronously with callBack :

const asyncDataFetchWithCallBack = async (url) => {
    try{
        const data = await fs.promises.readFile(url, {encoding:'utf8', flag:'r'});
        console.log(JSON.parse(data));
    }catch(err){
        console.log(err);
    }
}

asyncDataFetchWithCallBack(fileURL);

// parse its data and filter only the objects having age greater than 25

const asyncDataParseWithCallBack = async (url) => {
    try{        
        const data = await fs.promises.readFile(url, {encoding:'utf8', flag:'r'});
        const jsonData = JSON.parse(data)
        jsonData
            .filter(item => item.age > 25)
            .map(element =>{
                console.log(element);
            })
    }catch(err){
        console.error(err)
    }
}

asyncDataParseWithCallBack(fileURL);


// transform ‘firstName’ and ‘lastName’ to capitalize

const asyncTransformDataWithCallBack = async (url) => {
    try{        
        const data = await fs.promises.readFile(url, {encoding:'utf8', flag:'r'});
        const jsonData = JSON.parse(data)
        jsonData
            .filter(item => {
                item.firstName = item.firstName.charAt(0).toUpperCase() + item.firstName.slice(1);
                item.lastName = item.lastName.charAt(0).toUpperCase() + item.lastName.slice(1);                
                return item;
            })
            .map(item => console.log(item));
    }catch(err){
        console.error(err)
    }
}

asyncTransformDataWithCallBack(fileURL);


// update the file with this transformed data.


const asyncTransformDataAndUpdateWithCallBack = async (url) => {
    try{        
        const data = await fs.promises.readFile(url, {encoding:'utf8', flag:'r'});
        const jsonData = JSON.parse(data)
        const updatedData = jsonData.filter(item => {
                    item.firstName = item.firstName.charAt(0).toUpperCase() + item.firstName.slice(1);
                    item.lastName = item.lastName.charAt(0).toUpperCase() + item.lastName.slice(1);                
                    return item;
                });
        
        await fs.promises.writeFile(`./data/updates/UPDATED_PERSON_DATA_${new Date().getTime()}.json`, 
            JSON.stringify(updatedData),
            (err) => console.error(err));
    }catch(err){
        console.error(err)
    }
}

asyncTransformDataAndUpdateWithCallBack(fileURL);