const fs = require("fs");
const fileURL = "./data/PERSON_DATA.json";

// fetch data - Synchronously with Promises :

const syncDataFetchWithPromises = new Promise((resolve, reject) => {
  try {
    const data = fs.readFileSync(fileURL, { encoding: "utf8", flag: "r" });
    return resolve(JSON.parse(data));
  } catch (err) {
    return reject(err);
  }
});

syncDataFetchWithPromises
  .then((data) => console.log("synchronously with Promises (data):", data))
  .catch((err) => console.log("synchronously with Promises (err):", err));

// parse its data and filter only the objects having age greater than 25

const syncDataParseWithPromises = new Promise((resolve, reject) => {
  try {
    const data = fs.readFileSync(fileURL, { encoding: "utf8", flag: "r" });
    const parsedData = JSON.parse(data).filter((item) => item.age > 25);
    return resolve(parsedData);
  } catch (err) {
    return reject(err);
  }
});

syncDataParseWithPromises
  .then((data) => console.log("synchronously with Promises (data):", data))
  .catch((err) => console.log("synchronously with Promises (err):", err));

// transform ‘firstName’ and ‘lastName’ to capitalize

const syncTransformDataWithPromises = new Promise((resolve, reject) => {
  try {
    const data = fs.readFileSync(fileURL, { encoding: "utf8", flag: "r" });
    const updatedData = JSON.parse(data).filter((item) => {
      item.firstName =
        item.firstName.charAt(0).toUpperCase() + item.firstName.slice(1);
      item.lastName =
        item.lastName.charAt(0).toUpperCase() + item.lastName.slice(1);
      return item;
    });
    return resolve(updatedData);
  } catch (err) {
    return reject(err);
  }
});

syncTransformDataWithPromises
  .then((data) => console.log("synchronously with Promises (data):", data))
  .catch((err) => console.log("synchronously with Promises (err):", err));

// update the file with this transformed data.

const syncTransformDataAndUpdateWithPromises = new Promise(
  (resolve, reject) => {
    try {
      const data = fs.readFileSync(fileURL, { encoding: "utf8", flag: "r" });
      const updatedData = JSON.parse(data).filter((item) => {
        item.firstName =
          item.firstName.charAt(0).toUpperCase() + item.firstName.slice(1);
        item.lastName =
          item.lastName.charAt(0).toUpperCase() + item.lastName.slice(1);
        return item;
      });
      return resolve(updatedData);
    } catch (err) {
      return reject(err);
    }
  }
);

syncTransformDataAndUpdateWithPromises
  .then((data) =>
    fs.writeFileSync(
      `./data/updates/UPDATED_PERSON_DATA_${new Date().getTime()}.json`,
      JSON.stringify(data),
      (err) => console.error(err)
    )
  )
  .catch((err) => console.log("synchronously with Promises (err):", err));
