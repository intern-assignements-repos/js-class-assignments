const fs = require("fs");
const fileURL = "./data/PERSON_DATA.json";

// fetch data - Synchronously with callBack :

const syncDataFetchWithCallBack = (url) => {
    try{        
        const data = fs.readFileSync(url, {encoding:'utf8', flag:'r'});
        console.log(JSON.parse(data));
    }catch(err){
        console.error(err)
    }
}

// syncDataFetchWithCallBack(fileURL);

// parse its data and filter only the objects having age greater than 25

const syncDataParseWithCallBack = (url) => {
    try{        
        const data = fs.readFileSync(url, {encoding:'utf8', flag:'r'});
        const jsonData = JSON.parse(data)
        jsonData
            .filter(item => item.age > 25)
            .map(element =>{
                console.log(element);
            })
    }catch(err){
        console.error(err)
    }
}

// syncDataParseWithCallBack(fileURL);


// transform ‘firstName’ and ‘lastName’ to capitalize

const syncTransformDataWithCallBack = (url) => {
    try{        
        const data = fs.readFileSync(url, {encoding:'utf8', flag:'r'});
        const jsonData = JSON.parse(data)
        jsonData
            .filter(item => {
                item.firstName = item.firstName.charAt(0).toUpperCase() + item.firstName.slice(1);
                item.lastName = item.lastName.charAt(0).toUpperCase() + item.lastName.slice(1);                
                return item;
            })
            .map(item => console.log(item));
    }catch(err){
        console.error(err)
    }
}

// syncTransformDataWithCallBack(fileURL);


// update the file with this transformed data.


const syncTransformDataAndUpdateWithCallBack = (url) => {
    try{        
        const data = fs.readFileSync(url, {encoding:'utf8', flag:'r'});
        const jsonData = JSON.parse(data)
        const updatedData = jsonData.filter(item => {
                    item.firstName = item.firstName.charAt(0).toUpperCase() + item.firstName.slice(1);
                    item.lastName = item.lastName.charAt(0).toUpperCase() + item.lastName.slice(1);                
                    return item;
                });
        
        fs.writeFileSync(`./data/updates/UPDATED_PERSON_DATA_${new Date().getTime()}.json`, 
            JSON.stringify(updatedData),
            (err) => console.error(err));
    }catch(err){
        console.error(err)
    }
}

// syncTransformDataAndUpdateWithCallBack(fileURL);