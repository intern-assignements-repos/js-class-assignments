// Create 4 json files, utilize all 4 promise methods namely:
// Promise.all, promise.allSettled, promise.any, promise.race to read these files in parallel.

const {
  animalPromise,
  btcPromise,
  cityPromise,
  colorPromise,
} = require("./promises");


// Promise all :
Promise.all([animalPromise, btcPromise, cityPromise, colorPromise])
    .then((data) => console.log("Promise all (data):", data))
    .catch((err) => console.error("Promise all (error):", err));
  
  
// Promise allSettled : 
Promise.allSettled([animalPromise, btcPromise, cityPromise, colorPromise])
    .then((data) => console.log("Promise allSettled (data):", data))
    .catch((err) => console.error("Promise allSetteled (error):", err));
    

// Promise any :
Promise.any([animalPromise, btcPromise, cityPromise, colorPromise])
    .then((data) => console.log("Promise any (data):", data))
    .catch((err) => console.error("Promise any (error):", err));
    
    
// Promise race : 
Promise.race([animalPromise, btcPromise, cityPromise, colorPromise])
    .then((data) => console.log("Promise race (data):", data))
    .catch((err) => console.error("Promise race (error):", err));

