const fs = require("fs");
const animalFileURL = "./data/ANIMAL_DATA.json";
const btcFileURL = "./data/BTC_DATA.json";
const cityFileURL = "./data/CITY_DATA.json";
const colorFileURL = "./data/COLOR_DATA.json";

// Promise 1

const animalPromise = new Promise(async (resolve, reject) => {
  try {
    const data = await fs.promises.readFile(animalFileURL, {
      encoding: "utf8",
      flag: "r",
    });
    return resolve(JSON.parse(data));
  } catch (err) {
    return reject(err);
  }
});

// Promise 2

const btcPromise = new Promise(async (resolve, reject) => {
  try {
    const data = await fs.promises.readFile(btcFileURL, {
      encoding: "utf8",
      flag: "r",
    });
    return resolve(JSON.parse(data));
  } catch (err) {
    return reject(err);
  }
});

// Promise 3

const cityPromise = new Promise(async (resolve, reject) => {
  try {
    const data = await fs.promises.readFile(cityFileURL, {
      encoding: "utf8",
      flag: "r",
    });
    return resolve(JSON.parse(data));
  } catch (err) {
    return reject(err);
  }
});

// Promise 4

const colorPromise = new Promise(async (resolve, reject) => {
  try {
    const data = await fs.promises.readFile(colorFileURL, {
      encoding: "utf8",
      flag: "r",
    });
    return resolve(JSON.parse(data));
  } catch (err) {
    return reject(err);
  }
});


module.exports = {
    animalPromise,
    btcPromise,
    cityPromise,
    colorPromise
};