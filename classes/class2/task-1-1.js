// Print all even numbers from 10 - 100

for (let i = 10; i <= 100; i++) console.log(i % 2 === 0 ? i : "");