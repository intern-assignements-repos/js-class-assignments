// Group array of strings by first letter i.e. create subarrays having words which start with the same letter.

const arr = ["Group array of strings", "by first letter i.e.", "create subarrays having words", "which start with", "the same letter."];
const arr2 = ["Rahul", "Ravi", "Sumon", "Aman", "Sumit", "Amit", "Shakshi", "Parminder", "Ankit"]

const groupOfFirstLetterWords = (arr) => {    
    let res;
    let allWords = [];
    let letterDict = {};
    let index = 0;

    arr.forEach(element => {
        element.split(" ").forEach(item => allWords.push(item));
    });

    allWords.forEach(element => {
        let firstLetter = element[0].toLowerCase();
        if(letterDict[firstLetter] === undefined){
            letterDict[firstLetter] = index;
            index++;
        }
    });    

    res = new Array(allWords.length).fill(null).map(()=>new Array().fill(""));

    allWords.forEach(element => {
        let firstLetter = element[0].toLowerCase();        
        res[letterDict[firstLetter]].push(element)
    });

    res = res.filter(item => item.length !== 0);

    return res;
};

console.log(groupOfFirstLetterWords(arr2));