// Given a string possibly containing three types of braces ({}, [], ()), 
// write a function that returns a Boolean indicating whether the given string contains a valid nesting of braces.

const exp1 = "95 - [ 144 ÷ {(144) + (4 - 3) - 7 } ]";
// const exp1 = "[{()}]"

const checkBraces = (exp) => {    
    let stack = [];

    for(let i in exp){
        switch(exp[i]){
            case "[" :
                stack.push("[");
                break;
            case "{" :
                stack.push("{");
                break;
            case "(":
                stack.push("(");
                break;
            case ")":
                if(stack.pop()==="(")
                    break;
                else
                    return false;
            case "}":
                if(stack.pop()==="{")
                    break;
                else
                    return false;
            case "]":
                if(stack.pop()==="[")
                    break;
                else
                    return false;
            default:                
                break;
        }
    }

    return true;
};

console.log(checkBraces(exp1));