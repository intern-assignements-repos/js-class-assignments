// Merge two arrays with duplicate values and remove duplicates.

const arr1 = [1,2,3,4,5];
const arr2 = [4,5,6,7,8];

const mergeAndRemove = (arr1, arr2) => {
    let mergeSet = new Set();

    arr1.forEach(element => {
        mergeSet.add(element);
    });

    arr2.forEach(element => {
        mergeSet.add(element);
    })

    let res =[...mergeSet];

    return res;
};

console.log(mergeAndRemove(arr1, arr2));