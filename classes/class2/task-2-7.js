// Given an array of numbers, in which value of the element is equal to the number of positions you can jump. 
// Calculate the minimum number of jumps required to reach the end of the array.
// E.g. [1,3,4,8,2] outputs 2

const arr = [1,3,4,8,2];

const jumpsRequired = (arr) => {
    let jumps = 0;
    let distance = 0;
    let n = arr.length;
    let i = 0;
    while(distance<n) {        
        distance = i;        
        
        if(arr[i] === 0)
            return "Not Possible";
            
        jumps++; 
        i += arr[i] + 1;
        // console.log(i,":> jumps :", jumps, "; dis :", distance, "; value :", arr[i]);
    }    

    return jumps-1;
}

console.log(jumpsRequired(arr));