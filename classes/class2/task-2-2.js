// Sort a given array of person objects by their ‘firstName’.

const arr = [
  {
    firstName: "Ravi",
    lastName: "Kumar",
    age: "27",
  },

  {
    firstName: "Amit",
    lastName: "Kumar",
    age: "22",
  },

  {
    firstName: "Ritu",
    lastName: "Kumari",
    age: "26",
  },

  {
    firstName: "Sakshi",
    lastName: "Kumari",
    age: "24",
  },

  {
    firstName: "Mayur",
    lastName: "Kumar",
    age: "30",
  },

  {
    firstName: "Abhi",
    lastName: "Kumar",
    age: "21",
  },
];

const sortNamesArray = (arr) => {
  let tmp;
  for (var i = 0; i < arr.length; i++) {
    for (var j = i + 1; j < arr.length; j++) {
      if (arr[i] > arr[j]) {
        tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
      }
    }
  }
  return arr;
};

const sortArrayFirstName = (arr) => {
  let res = new Array(arr.length).fill(null);
  let firstNameArray = [];

  arr.forEach((element) => {
    firstNameArray.push(element.firstName);
  });

  const sortedFirstNameArray = sortNamesArray(firstNameArray);

  arr.forEach((item) => {
    res[sortedFirstNameArray.indexOf(item.firstName)] = item;
  });

  return res;
};

console.log(sortArrayFirstName(arr));
