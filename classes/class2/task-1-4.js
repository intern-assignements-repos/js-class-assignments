// Calculate the sum of odd numbers greater than 10 and multiples of 3 from a given array

const arr = [11, 23, 25, 27, 90, 30, 33, 21, 66, 78, 17];

const calculate = (arr) => {    
    let sum = 0;

    arr.forEach(element => {
        sum += (element > 10 && element % 2 !== 0 && element % 3 === 0 ? element : 0);
    });

    return console.log(sum);
};

calculate(arr);