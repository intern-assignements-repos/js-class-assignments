// Given an array of numbers and sum, find the pairs of elements in the array having the sum.
// E.g. [1, 9, 2, 5, 3] and 5 ===> (2,3)

const arr = [1, 9, 2, 5, 3];
const sum = 5;

const findPairSum = (arr, sum) => {
    let res = [];

    for(let i = 0; i<arr.length; i++){
        for(let j = i + 1; j<arr.length; j++){
            if(arr[i]+arr[j]===sum)
                res.push(arr[i], arr[j]);
        }
    }

    return res;
}

console.log(findPairSum(arr, sum));