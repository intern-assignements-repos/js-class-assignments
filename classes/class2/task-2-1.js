// Given an array of person objects, fetch only the objects having age greater than 25 and capitalize their firstName attribute.

const arr = [
  {
    firstName: "ravi",
    lastName: "Kumar",
    age: "27",
  },

  {
    firstName: "Amit",
    lastName: "Kumar",
    age: "22",
  },

  {
    firstName: "ritu",
    lastName: "Kumari",
    age: "26",
  },

  {
    firstName: "sakshi",
    lastName: "Kumari",
    age: "24",
  },

  {
    firstName: "mayur",
    lastName: "Kumar",
    age: "30",
  },

  {
    firstName: "Abhi",
    lastName: "Kumar",
    age: "21",
  },
];

console.log(arr.filter(element => element.age > 25).map(element => {
    element.firstName = element.firstName.toUpperCase();
    return element;
}));
